import click
import spoty.deezer_api
import spoty.utils
from spoty.deezer_api import get_dz
from spoty import log
from deemix import generateDownloadObject
from deemix.settings import load as loadSettings
from deemix.utils import getBitrateNumberFromText, formatListener
import deemix.utils.localpaths as localpaths
from deemix.itemgen import GenerationError
from deemix.downloader import *

import os


class LogListener:
    def send(cls, key, value=None):
        logString = formatListener(key, value)
        if logString:
            # print(logString)
            log.debug(str(logString).encode('utf-8'))


def download(url, bitrate='flac', portable=False, path=None):
    # Check for local configFolder
    localpath = Path('')
    configFolder = localpath / 'config' if portable else localpaths.getConfigFolder()

    settings = loadSettings(configFolder)

    listener = LogListener()

    plugins = {}

    def downloadLinks(url, bitrate=None):
        if not bitrate: bitrate = settings.get("maxBitrate", TrackFormats.MP3_320)
        links = []
        for link in url:
            if ';' in link:
                for l in link.split(";"):
                    links.append(l)
            else:
                links.append(link)

        downloadObjects = []

        for link in links:
            try:
                downloadObject = generateDownloadObject(get_dz(), link, bitrate, plugins, listener)
            except GenerationError as e:
                print(f"{e.link}: {e.message}")
                continue
            if isinstance(downloadObject, list):
                downloadObjects += downloadObject
            else:
                downloadObjects.append(downloadObject)

        for obj in downloadObjects:
            if obj.__type__ == "Convertable":
                obj = plugins[obj.plugin].convert(get_dz(), obj, settings, listener)
            Downloader(get_dz(), obj, settings, listener).start()

    if path is not None:
        if path == '': path = ''
        path = Path(path)
        settings['downloadLocation'] = str(path)
    url = list(url)
    if bitrate: bitrate = getBitrateNumberFromText(bitrate)

    # If first url is filepath readfile and use them as URLs
    try:
        isfile = Path(url[0]).is_file()
    except Exception:
        isfile = False
    if isfile:
        filename = url[0]
        with open(filename) as f:
            url = f.readlines()

    downloadLinks(url, bitrate)


def playlist_download(playlist_id, path):
    log.info(f'Downloading playlist {playlist_id} to path "{path}"')

    # deezy.downloader.download([f'https://www.deezer.com/en/playlist/{playlist_id}'], 'flac', False, path)

    tracks, playlist = spoty.deezer_api.get_playlist_with_full_list_of_tracks(playlist_id)

    playlist_name = spoty.utils.slugify_file_pah(playlist['TITLE'])
    path = os.path.join(path, playlist_name)

    click.echo(f'Downloading {len(tracks)} tracks from playlist "{playlist["TITLE"]}"')

    with click.progressbar(tracks, label='Downloading playlist') as bar:
        for track in bar:
            download([f'https://www.deezer.com/en/track/{track["SNG_ID"]}'], 'flac', False, path)

    log.success(f'Playlist {playlist_id} downloaded to path "{path}"')


def track_download(track_id, path):
    log.info(f'Downloading tracks {track_id} to path "{path}"')
    download([f'https://www.deezer.com/en/track/{track_id}'], 'flac', False, path)
    log.success(f'Tracks {track_id} downloaded to path "{path}"')
