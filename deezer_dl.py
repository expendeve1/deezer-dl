from spoty import log
from spoty import settings
import spoty.utils
import spoty.deezer_api
import click
import re
import time
import os
import spoty.plugins.deezer_dl.deezer_downloader as deezer_downloader
from threading import Thread, Lock
from typing import List
from spoty import plugins_path
import sys

# benchmark (16 tracks) - seconds
# 1  thread  - 65
# 2  threads - 44
# 4  threads - 38
# 8  threads - 37
# 16  threads - 37

THREADS_COUNT = 4


class TrackToDownload:
    track_id: str
    path: str


downloading_threads = {}
mutex = Lock()
tracks_downloaded_file_name = os.path.join(plugins_path, 'deezer_dl', 'tracks_downloaded.txt')
tracks_to_download_file_name = os.path.join(plugins_path, 'deezer_dl', 'tracks_to_download.txt')
aborted = False


@click.group()
def deezer_dl():
    """
Plugin for downloading music from Deezer.
    """
    pass


@deezer_dl.command("download-playlists")
@click.argument("playlist_ids", type=str, nargs=-1)
@click.option('--path', type=str, default=settings.SPOTY.DEFAULT_EXPORT_PATH, help='Path to download files')
def download_playlists(playlist_ids, path):
    """
Download specified playlists.

PLAYLIST_IDS - list of playlist IDs
    """

    last_tracks_to_download, _ = read_tracks_to_download()
    if len(last_tracks_to_download) > 0:
        click.confirm("The last download has not been completed. Are you sure you want to start new?", abort=True)

    path = os.path.abspath(path)

    if len(playlist_ids) == 0:
        exit()

    tracks_to_download: List[TrackToDownload] = []

    with click.progressbar(playlist_ids, label=f'Reading {len(playlist_ids)} playlists') as bar:
        for playlist_id in playlist_ids:

            tracks, playlist = spoty.deezer_api.get_playlist_with_full_list_of_tracks(playlist_id)

            playlist_name = spoty.utils.slugify_file_pah(playlist['TITLE'])
            playlist_path = os.path.join(path, playlist_name)

            for track in tracks:
                t = TrackToDownload()
                t.track_id = track['SNG_ID']
                t.path = playlist_path
                tracks_to_download.append(t)

    write_tracks_to_download(tracks_to_download)
    download_tracks(tracks_to_download)



@deezer_dl.command("download-all")
@click.option('--path', type=str, default=settings.SPOTY.DEFAULT_EXPORT_PATH, help='Path to create files')
@click.option('--filter-playlists', type=str, default=None,
              help='Download only playlists whose names matches this regex filter')
@click.option('--user_id', type=str, default=None, help='Get playlists of this user')
@click.option('--confirm', '-y', type=bool, is_flag=True, default=False,
              help='Do not ask for export confirmation')
def download_all(path, filter_playlists, user_id, confirm):
    """
Download all playlists from user library.
    """

    last_tracks_to_download, _ = read_tracks_to_download()
    if len(last_tracks_to_download) > 0:
        click.confirm("The last download has not been completed. Are you sure you want to start new?", abort=True)

    path = os.path.abspath(path)

    if user_id == None:
        playlists = spoty.deezer_api.get_list_of_user_playlists()
        click.echo(f'You have {len(playlists)} playlists')
    else:
        playlists = spoty.deezer_api.get_list_of_user_playlists(user_id)
        click.echo(f'User has {len(playlists)} playlists')

    if len(playlists) == 0:
        exit()

    if filter_playlists is not None:
        playlists = list(filter(lambda pl: re.findall(filter_playlists, pl['title']), playlists))
        click.echo(f'{len(playlists)} playlists matches the filter')

    if len(playlists) == 0:
        exit()

    total_tracks = 0
    for playlist in playlists:
        total_tracks += int(playlist['nb_tracks'])

    click.echo(f'The following playlists will be downloaded:')
    for playlist in playlists:
        click.echo("  " + playlist['title'])

    click.echo(f'Total {total_tracks} tracks in {len(playlists)} playlists')
    click.echo(f'Downloading path: {path}')

    if not confirm:
        click.confirm('Do you want to download this tracks?', abort=True)

    tracks_to_download: List[TrackToDownload] = []

    with click.progressbar(playlists, label=f'Reading {len(playlists)} playlists') as bar:
        for playlist in bar:
            playlist_id = playlist['id']

            tracks, playlist = spoty.deezer_api.get_playlist_with_full_list_of_tracks(playlist_id)

            playlist_name = spoty.utils.slugify_file_pah(playlist['TITLE'])
            playlist_path = os.path.join(path, playlist_name)

            for track in tracks:
                t = TrackToDownload()
                t.track_id = track['SNG_ID']
                t.path = playlist_path
                tracks_to_download.append(t)

    write_tracks_to_download(tracks_to_download)
    download_tracks(tracks_to_download)


@deezer_dl.command("download-continue")
def download_continue():
    """
Continue last download.
    """
    global to_download_count, complete_count

    new_tracks_to_download, all_tracks_to_download = read_tracks_to_download()
    if len(new_tracks_to_download) == 0:
        exit()

    to_download_count = len(all_tracks_to_download)
    complete_count = to_download_count - len(new_tracks_to_download)

    download_tracks(new_tracks_to_download, to_download_count, complete_count)


def write_tracks_to_download(tracks_to_download: List[TrackToDownload]):
    global tracks_downloaded_file_name
    global tracks_to_download_file_name

    with open(tracks_downloaded_file_name, 'w') as f:
        f.write("")

    with open(tracks_to_download_file_name, 'w') as f:
        for track in tracks_to_download:
            f.write(f'{track.track_id};{track.path}\n')


def clean_tracks_to_download_file():
    with open(tracks_downloaded_file_name, 'w') as f:
        f.write("")

    with open(tracks_to_download_file_name, 'w') as f:
        f.write("")


def write_track_downloaded(track_id):
    global tracks_downloaded_file_name

    with open(tracks_downloaded_file_name, 'a') as f:
        f.write(track_id + '\n')


def read_tracks_to_download():
    global tracks_downloaded_file_name
    global tracks_to_download_file_name

    all_tracks_to_download = []
    if os.path.isfile(tracks_to_download_file_name):
        with open(tracks_to_download_file_name, 'r') as f:
            lines = f.readlines()
            for line in lines:
                t = TrackToDownload()
                t.track_id = line.split(';')[0]
                t.path = line.split(';')[1].strip()
                all_tracks_to_download.append(t)

    if os.path.isfile(tracks_downloaded_file_name):
        with open(tracks_downloaded_file_name, 'r') as f:
            downloaded_track_ids = f.readlines()
            for i, line in enumerate(downloaded_track_ids):
                downloaded_track_ids[i] = line.strip()

    new_tracks_to_download = []
    for track in all_tracks_to_download:
        if track.track_id not in downloaded_track_ids:
            new_tracks_to_download.append(track)

    return new_tracks_to_download, all_tracks_to_download


downloading_bar = None
to_download_count = 0
complete_count = 0


def download_tracks(tracks_to_download: List[TrackToDownload], total_tracks_count=None, tracks_complete_count=None):
    global downloading_threads, THREADS_COUNT, downloading_bar, to_download_count, complete_count, aborted

    if total_tracks_count is None:
        to_download_count = len(tracks_to_download)
    else:
        to_download_count = total_tracks_count

    if tracks_complete_count is None:
        complete_count = 0
    else:
        complete_count = tracks_complete_count

    try:
        with click.progressbar(length=to_download_count,
                               label=f'Downloading track {complete_count + 1}/{to_download_count}') as bar:
            downloading_bar = bar
            bar.update(complete_count)

            for i in range(len(tracks_to_download)):
                track_id = tracks_to_download[i].track_id
                playlist_path = tracks_to_download[i].path

                thread = Thread(target=download, args=(track_id, playlist_path, i))
                # thread.daemon = True # This thread dies when main thread exits
                with mutex:
                    downloading_threads[i] = thread
                thread.start()

                if len(downloading_threads) < THREADS_COUNT:
                    continue

                while len(downloading_threads) >= THREADS_COUNT:
                    time.sleep(0.1)

                if i == len(tracks_to_download) - 1:  # waiting for the end
                    while len(downloading_threads) > 0:
                        time.sleep(0.1)

        clean_tracks_to_download_file()
        click.echo(f'Downloading complete')
        log.success(f'Downloading complete')
    except (KeyboardInterrupt, SystemExit):  # aborted by user
        click.echo()
        click.echo('Aborted.')
        with mutex:
            click.echo(f'Waiting for the current {len(downloading_threads)} downloads to complete...')
        aborted = True
        sys.exit()


def download(track_id, playlist_path, thread_id):
    global downloading_threads, downloading_bar, to_download_count, complete_count

    # print(f'Downloading {track_id}')
    deezer_downloader.download([f'https://www.deezer.com/en/track/{track_id}'], 'flac', False,
                               playlist_path)
    # print(f'Downloaded {track_id}')

    with mutex:
        if not aborted:
            write_track_downloaded(track_id)
            complete_count += 1

            if downloading_bar is not None:
                downloading_bar.update(1)
                downloading_bar.label = f'Downloading track {complete_count + 1}/{to_download_count}'

            if thread_id in downloading_threads:
                del downloading_threads[thread_id]
